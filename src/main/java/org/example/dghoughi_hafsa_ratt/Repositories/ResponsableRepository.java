package org.example.dghoughi_hafsa_ratt.Repositories;

import org.example.dghoughi_hafsa_ratt.Entities.Responsable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResponsableRepository extends JpaRepository<Responsable,Long> {
}
