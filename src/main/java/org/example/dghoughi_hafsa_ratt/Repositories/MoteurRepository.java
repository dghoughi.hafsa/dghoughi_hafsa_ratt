package org.example.dghoughi_hafsa_ratt.Repositories;

import org.example.dghoughi_hafsa_ratt.Entities.Moteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoteurRepository extends JpaRepository<Moteur,Integer> {
}
