package org.example.dghoughi_hafsa_ratt.Repositories;

import org.example.dghoughi_hafsa_ratt.Entities.Defaut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DefaultRepository extends JpaRepository<Defaut,Integer> {
}
