package org.example.dghoughi_hafsa_ratt.Repositories;

import org.example.dghoughi_hafsa_ratt.Entities.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployerRepository extends JpaRepository<Employe,Long> {
    Optional<Employe> findById(Long id);
    List<Employe> findAll();

}
