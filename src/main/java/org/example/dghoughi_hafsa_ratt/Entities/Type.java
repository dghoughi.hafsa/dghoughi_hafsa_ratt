package org.example.dghoughi_hafsa_ratt.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer code;
    private String nom;

    @OneToMany(mappedBy = "type")
    @JsonIgnore
    public List<Defaut> defauts;

    public Type() {
    }

    public Type(String nom) {
        this.nom = nom;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Type{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                '}';
    }
}
