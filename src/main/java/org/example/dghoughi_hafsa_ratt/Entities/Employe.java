package org.example.dghoughi_hafsa_ratt.Entities;

import jakarta.persistence.*;
import org.springframework.security.core.userdetails.UserDetails;

import javax.annotation.processing.Generated;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Employe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    protected String nom;
    private String prenom;
    private Date dateNaissance;
    private String fonction;

    @ManyToOne
    @JoinColumn(name = "EmpUsine", referencedColumnName ="idUsine", nullable = false)
    public Usine usineEmp;

    public Employe() {
    }

    public Employe(String nom, String prenom, Date dateNaissance, String fonction) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.fonction = fonction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", fonction='" + fonction + '\'' +
                '}';
    }
}
