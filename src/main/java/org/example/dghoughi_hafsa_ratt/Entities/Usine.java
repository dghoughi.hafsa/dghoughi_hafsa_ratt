package org.example.dghoughi_hafsa_ratt.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
public class Usine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUsine;
    protected String nomUsine;
    private String pays;
    private String ville;

    @OneToMany(mappedBy = "usine")
    @JsonIgnore
    public List<Modele> modeles;

    @OneToMany(mappedBy = "usineEmp")
    @JsonIgnore
    public List<Employe> employeList;


    public Usine() {
    }

    public Usine(String nomUsine, String pays, String ville) {
        this.nomUsine = nomUsine;
        this.pays = pays;
        this.ville = ville;
    }


    public Long getIdUsine() {
        return idUsine;
    }

    public void setIdUsine(Long idUsine) {
        this.idUsine = idUsine;
    }

    public String getNomUsine() {
        return nomUsine;
    }

    public void setNomUsine(String nomUsine) {
        this.nomUsine = nomUsine;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Override
    public String toString() {
        return "Usine{" +
                "idUsine=" + idUsine +
                ", nomUsine='" + nomUsine + '\'' +
                ", pays='" + pays + '\'' +
                ", ville='" + ville + '\'' +
                '}';
    }
}
