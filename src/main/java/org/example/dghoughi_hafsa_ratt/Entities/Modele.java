package org.example.dghoughi_hafsa_ratt.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
public class Modele {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idModele;
    protected String code;

    private Integer poids;
    private Date dateFabrication;
    private Date finFabrication;

    public Modele() {
    }

    public Modele(String code, Integer poids, Date dateFabrication, Date finFabrication) {
        this.code = code;
        this.poids = poids;
        this.dateFabrication = dateFabrication;
        this.finFabrication = finFabrication;
    }

    @ManyToOne
    @JoinColumn(name = "modeleUsine", referencedColumnName = "idUsine", nullable = false)
    public Usine usine;

    @OneToMany(mappedBy = "modele")
    @JsonIgnore
    public List<Moteur> moteurs;

    @ManyToOne
    @JoinColumn(name = "modeleR", nullable = false)
    public Responsable responsable;

    public Long getIdModele() {
        return idModele;
    }

    public void setIdModele(Long idModele) {
        this.idModele = idModele;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getPoids() {
        return poids;
    }

    public void setPoids(Integer poids) {
        this.poids = poids;
    }

    public Date getDateFabrication() {
        return dateFabrication;
    }

    public void setDateFabrication(Date dateFabrication) {
        this.dateFabrication = dateFabrication;
    }

    public Date getFinFabrication() {
        return finFabrication;
    }

    public void setFinFabrication(Date finFabrication) {
        this.finFabrication = finFabrication;
    }

    @Override
    public String toString() {
        return "Modele{" +
                "idModele=" + idModele +
                ", code='" + code + '\'' +
                ", poids=" + poids +
                ", dateFabrication=" + dateFabrication +
                ", finFabrication=" + finFabrication +
                '}';
    }
}
