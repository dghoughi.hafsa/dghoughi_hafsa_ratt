package org.example.dghoughi_hafsa_ratt.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
public class Defaut {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer num;
    private Date date;
    private String  description;
    private Integer gravite;
    private Integer kilometrage;

    @ManyToOne
    @JoinColumn(name = "defaultM", nullable = false)
    public Modele modele;

    @ManyToOne
    @JoinColumn(name = "typeD", referencedColumnName ="code")
    public Type type;

    public Defaut() {
    }

    @ManyToOne
    @JoinColumn(name = "defaultOp", referencedColumnName = "id")
    public Operateur operateur;
    @ManyToOne
    @JoinColumn(name = "moteurDefaut")
    public Moteur moteur;


    public Defaut(Date date, String description, Integer gravite, Integer kilometrage) {
        this.date = date;
        this.description = description;
        this.gravite = gravite;
        this.kilometrage = kilometrage;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGravite() {
        return gravite;
    }

    public void setGravite(Integer gravite) {
        this.gravite = gravite;
    }

    public Integer getKilometrage() {
        return kilometrage;
    }

    public void setKilometrage(Integer kilometrage) {
        this.kilometrage = kilometrage;
    }

    @Override
    public String toString() {
        return "Defaut{" +
                "num=" + num +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", gravite=" + gravite +
                ", kilometrage=" + kilometrage +
                '}';
    }
}
