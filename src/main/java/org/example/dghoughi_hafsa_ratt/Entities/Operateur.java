package org.example.dghoughi_hafsa_ratt.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

import java.util.Date;
import java.util.List;

@Entity
public class Operateur extends Employe{
    @OneToMany(mappedBy = "operateur")
    @JsonIgnore
    public List<Defaut > defautListOp;

    public Operateur() {
    }

    public Operateur(String nom, String prenom, Date dateNaissance, String fonction) {
        super(nom, prenom, dateNaissance, fonction);
    }
}
