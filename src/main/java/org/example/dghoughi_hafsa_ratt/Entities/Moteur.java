package org.example.dghoughi_hafsa_ratt.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.springframework.boot.Banner;

import java.util.Date;
import java.util.List;

@Entity
public class Moteur  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer numSerie;
    private Integer kilometrage;
    private Date miseEnService;

    @ManyToOne
    @JoinColumn(name = "moteurM", referencedColumnName = "idModele", nullable = false)
    public Modele modele;



    public Moteur() {
    }

    public Moteur(Long idMoteur, Integer numSerie, Integer kilometrage, Date miseEnService) {

        this.numSerie = numSerie;
        this.kilometrage = kilometrage;
        this.miseEnService = miseEnService;
    }





    public Integer getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(Integer numSerie) {
        this.numSerie = numSerie;
    }

    public Integer getKilometrage() {
        return kilometrage;
    }

    public void setKilometrage(Integer kilometrage) {
        this.kilometrage = kilometrage;
    }

    public Date getMiseEnService() {
        return miseEnService;
    }

    public void setMiseEnService(Date miseEnService) {
        this.miseEnService = miseEnService;
    }

    @Override
    public String toString() {
        return "Moteur{" +
                ", numSerie=" + numSerie +
                ", kilometrage=" + kilometrage +
                ", miseEnService=" + miseEnService +
                '}';
    }
}
