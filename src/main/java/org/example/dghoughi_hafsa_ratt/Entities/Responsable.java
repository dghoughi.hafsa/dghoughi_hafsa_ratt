package org.example.dghoughi_hafsa_ratt.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;

import java.util.Date;
import java.util.List;

@Entity
public class Responsable extends Employe {
    @OneToMany(mappedBy = "responsable")
    @JsonIgnore
    public List<Modele> modeles;

    public Responsable() {
    }

    public Responsable(String nom, String prenom, Date dateNaissance, String fonction) {
        super(nom, prenom, dateNaissance, fonction);
    }
}
