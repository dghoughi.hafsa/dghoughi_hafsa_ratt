package org.example.dghoughi_hafsa_ratt;

import org.example.dghoughi_hafsa_ratt.Controllers.EmployeController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DghoughiHafsaRattApplication {
    @Autowired
    private EmployeController employeController;

    public DghoughiHafsaRattApplication(EmployeController employeController) {
        this.employeController = employeController;
    }

    public static void main(String[] args) {
        SpringApplication.run(DghoughiHafsaRattApplication.class, args);
    }

    CommandLineRunner run() {
        return args -> {


        };

    }
    @Bean
    public CommandLineRunner commandLineRunner(EmployeController employeController) {
        return (args) -> {

            Employe employeCree = new Employe();
            employeCree.setNom("John");
            employeCree.setPrenom("Doe");
            employeCree.setFonction("technicien");
            employeCree = employeController.addEmploye(employeCree);
            System.out.println("Employé créé:");
            System.out.println(employeController.findEmployeById(employeCree.getId()));


            Employe employeModifie = new Employe();
            employeModifie.setNom("Jane");
            employeModifie.setPrenom("Doe");
            employeController.updateEmploye(employeModifie, employeCree.getId());
            System.out.println("Employé modifié:");
            System.out.println(employeController.findEmployeById(employeCree.getId()));


            employeController.deleteEmploye(employeCree.getId());
            System.out.println("Employé supprimé.");


            System.out.println(employeController.findEmployeById(employeCree.getId()));
        };
    }
}