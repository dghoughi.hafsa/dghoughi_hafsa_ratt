package org.example.dghoughi_hafsa_ratt.Controllers;


import org.example.dghoughi_hafsa_ratt.Entities.Operateur;

import org.example.dghoughi_hafsa_ratt.Repositories.OperateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/operateur")
public class OperateurController {
    private final OperateurRepository operateurRepository;
    @Autowired
    public OperateurController(OperateurRepository operateurRepository) {
        this.operateurRepository = operateurRepository;
    }


    @GetMapping
    public List<Operateur> findAll(){
        return operateurRepository.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Operateur> findOperateurById(@PathVariable Long id){
        return operateurRepository.findById(id);
    }
    @PostMapping("/add")
    public Operateur addOperateur(@RequestBody Operateur operateur){
        return operateurRepository.save(operateur);
    }


    @PutMapping("/update/{id}")
    public void updateOperateur(@RequestBody Operateur operateur , @PathVariable Long id){
        operateurRepository.findById(id).map((operateur1)->{
            operateur1.setNom(operateur.getNom());
            return operateurRepository.save(operateur);
        });
    }

    @DeleteMapping("/delete/{OperateurId}")
    public void deleteOperateur(@PathVariable Long id){
        operateurRepository.deleteById(id);
    }

}
