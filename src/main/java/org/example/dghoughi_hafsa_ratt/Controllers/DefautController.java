package org.example.dghoughi_hafsa_ratt.Controllers;

import org.example.dghoughi_hafsa_ratt.Entities.Defaut;
import org.example.dghoughi_hafsa_ratt.Repositories.DefaultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/defaut")
public class DefautController {
    private final DefaultRepository defaultRepository;
    @Autowired
    public DefautController(DefaultRepository defaultRepository) {
        this.defaultRepository = defaultRepository;
    }

    @GetMapping
    public List<Defaut> findAll(){
        return defaultRepository.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Defaut> findDefautById(@PathVariable Integer num){
        return defaultRepository.findById(num);
    }
    @PostMapping("/add")
    public Defaut addDefaut(@RequestBody Defaut defaut){
        return defaultRepository.save(defaut);
    }


    @PutMapping("/update/{id}")
    public void updateDefaut(@RequestBody Defaut defaut, @PathVariable Integer num){
        defaultRepository.findById(num).map((defaut1)->{
            defaut1.setNum(defaut.getNum());
            return defaultRepository.save(defaut1);
        });
    }

    @DeleteMapping("/delete/{num}")
    public void deleteDefaut(@PathVariable Integer num){
        defaultRepository.deleteById(num);
    }

}
