package org.example.dghoughi_hafsa_ratt.Controllers;


import org.example.dghoughi_hafsa_ratt.Entities.Type;
import org.example.dghoughi_hafsa_ratt.Repositories.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/type")
public class TypeController {
    private final TypeRepository typeRepository;
    @Autowired
    public TypeController(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    @GetMapping
    public List<Type> findAll(){
        return typeRepository.findAll();
    }
    @GetMapping("/{code}")
    public Optional<Type> findTypeById(@PathVariable Integer code){
        return typeRepository.findById(code);
    }
    @PostMapping("/add")
    public Type addType(@RequestBody Type type){
        return typeRepository.save(type);
    }


    @PutMapping("/update/{id}")
    public void updateType(@RequestBody Type type, @PathVariable Integer code){
        typeRepository.findById(code).map((type1)->{
            type1.setNom(type.getNom());
            return typeRepository.save(type1);
        });
    }

    @DeleteMapping("/delete/{personneId}")
    public void deleteType(@PathVariable Integer code){
        typeRepository.deleteById(code);
    }

}
