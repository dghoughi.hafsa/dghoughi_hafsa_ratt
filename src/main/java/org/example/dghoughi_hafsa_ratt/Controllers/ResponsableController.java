package org.example.dghoughi_hafsa_ratt.Controllers;

import org.example.dghoughi_hafsa_ratt.Entities.Responsable;
import org.example.dghoughi_hafsa_ratt.Repositories.ResponsableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/respo")
public class ResponsableController {
    private final ResponsableRepository responsableRepository ;
    @Autowired
    public ResponsableController(ResponsableRepository responsableRepository) {
        this.responsableRepository = responsableRepository;
    }



    @GetMapping
    public List<Responsable> findAll(){
        return responsableRepository.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Responsable> findRespoById(@PathVariable Long idR){
        return responsableRepository.findById(idR);
    }
    @PostMapping("/add")
    public Responsable addRespo(@RequestBody Responsable responsable){
        return responsableRepository.save(responsable);
    }




    @PutMapping("/update/{id}")
    public void updateRespo(@RequestBody Responsable responsable, @PathVariable Long idR){
        responsableRepository.findById(idR).map((responsable1)->{
            responsable1.setNom(responsable.getNom());
            return responsableRepository.save(responsable1);
        });
    }

    @DeleteMapping("/delete/{respoId}")
    public void deleteRespo(@PathVariable Long idR){
        responsableRepository.deleteById(idR);
    }

}
