package org.example.dghoughi_hafsa_ratt.Controllers;


import org.example.dghoughi_hafsa_ratt.Entities.Modele;
import org.example.dghoughi_hafsa_ratt.Repositories.ModeleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/modele")
public class ModeleController {
    private final ModeleRepository modeleRepository;
    @Autowired
    public ModeleController(ModeleRepository modeleRepository) {
        this.modeleRepository = modeleRepository;
    }

    @GetMapping
    public List<Modele> findAll(){
        return modeleRepository.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Modele> findModeleById(@PathVariable Long code){
        return modeleRepository.findById(code);
    }
    @PostMapping("/add")
    public Modele addModele(@RequestBody Modele modele){
        return modeleRepository.save(modele);
    }


    @PutMapping("/update/{id}")
    public void updateModele(@RequestBody Modele modele, @PathVariable Long code){
        modeleRepository.findById(code).map((modele1)->{
            modele1.setCode(modele.getCode());
            return modeleRepository.save(modele1);
        });
    }

    @DeleteMapping("/delete/{code}")
    public void deleteModele(@PathVariable Long code){
        modeleRepository.deleteById(code);
    }

}
