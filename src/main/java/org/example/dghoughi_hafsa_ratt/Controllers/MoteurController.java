package org.example.dghoughi_hafsa_ratt.Controllers;



import org.example.dghoughi_hafsa_ratt.Entities.Moteur;
import org.example.dghoughi_hafsa_ratt.Repositories.MoteurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/moteur")
public class MoteurController {
    private final MoteurRepository moteurRepository;
    @Autowired
    public MoteurController(MoteurRepository moteurRepository) {
        this.moteurRepository = moteurRepository;

    }

    @GetMapping
    public List<Moteur> findAll(){
        return moteurRepository.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Moteur> findMoteurById(@PathVariable Integer numSerie){
        return moteurRepository.findById(numSerie);
    }
    @PostMapping("/add")
    public Moteur addMoteur(@RequestBody Moteur moteur){
        return moteurRepository.save(moteur);
    }


    @PutMapping("/update/{id}")
    public void updateMoteur(@RequestBody Moteur moteur, @PathVariable Integer numSerie){
        moteurRepository.findById(numSerie).map((moteur1)->{
            moteur1.setNumSerie(moteur.getNumSerie());
            return moteurRepository.save(moteur1);
        });
    }

    @DeleteMapping("/delete/{code}")
    public void deleteMoteur(@PathVariable Integer numSerie){
        moteurRepository.deleteById(numSerie);
    }

}
