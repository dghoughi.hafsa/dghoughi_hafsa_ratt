package org.example.dghoughi_hafsa_ratt.Controllers;


import org.example.dghoughi_hafsa_ratt.Entities.Usine;
import org.example.dghoughi_hafsa_ratt.Repositories.UsineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usine")
public class UsineController {
    private final UsineRepository usineRepository;
    @Autowired
    public UsineController(UsineRepository usineRepository) {
        this.usineRepository = usineRepository;
    }

    @GetMapping
    public List<Usine> findAll(){
        return usineRepository.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Usine> findUsineById(@PathVariable Long id){
        return usineRepository.findById(id);
    }
    @PostMapping("/add")
    public Usine addUsine(@RequestBody Usine usine){
        return usineRepository.save(usine);
    }


    @PutMapping("/update/{id}")
    public void updateUsine(@RequestBody Usine usine, @PathVariable Long id){
        usineRepository.findById(id).map((usine1)->{
            usine1.setNomUsine(usine.getNomUsine());
            return usineRepository.save(usine1);
        });
    }

    @DeleteMapping("/delete/{UsineId}")
    public void deleteUsine(@PathVariable Long id){
        usineRepository.deleteById(id);
    }


}
