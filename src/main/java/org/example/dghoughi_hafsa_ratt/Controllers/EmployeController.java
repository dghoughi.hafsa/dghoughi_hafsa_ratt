package org.example.dghoughi_hafsa_ratt.Controllers;

import org.example.dghoughi_hafsa_ratt.Entities.Employe;
import org.example.dghoughi_hafsa_ratt.Repositories.EmployerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employe")
public class EmployeController {
    private final EmployerRepository employerRepository;
    @Autowired

    public EmployeController(EmployerRepository employerRepository) {
        this.employerRepository = employerRepository;
    }
    @GetMapping
    public List<Employe> findAll(){
        return employerRepository.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Employe> findEmployeById(@PathVariable Long id){
        return employerRepository.findById(id);
    }
    @PostMapping("/add")
    public Employe addEmploye(@RequestBody Employe employe){
        return employerRepository.save(employe);
    }


    @PutMapping("/update/{id}")
    public void updateEmploye(@RequestBody Employe employe, @PathVariable Long id){
        employerRepository.findById(id).map((employe1)->{
            employe1.setNom(employe.getNom());
            return employerRepository.save(employe1);
        });
    }

    @DeleteMapping("/delete/{personneId}")
    public void deleteEmploye(@PathVariable Long id){
        employerRepository.deleteById(id);
    }

}

